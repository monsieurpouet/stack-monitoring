# What ?

This git repository is a stack to deploy a some server with:
* Treafik
* Zabbix
* Zabbix Agent
* Zabbix Database
* Grafana
* Gotify 

## Installation

### Requirements

- git
- docker-compose
  

## Usage

```bash
git clone https://framagit.org/monsieurpouet/stack-monitoring.git

docker-compose up -d
```

In the docker-compose file, you have to change some variables.

- **TREAFIK_PATH**: The path about the traefik configuration file
- **GOTIFY_PATH**: The path about the gotify configuration file
- **ZABBIX_PATH**: The path about the zabbix configuration file
- **DOMAIN**: Your domain

In the traefik config file, you must configure:

* email
* create .htpasswd in /etc/docker/traefik/.htpasswd with

```bash
htpasswd -nb [USER] [PASSWORD YOU WANT]
```

Don't forget to change the sub-domain for each services and change the zabbix database password in .env_zabbix.

You can add plugins for the installation in .env_grafana at GF_INSTALL_PLUGINS.

